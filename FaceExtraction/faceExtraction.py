'''
KAIST CS489 Team 6
faceExtraction.py
Main Author: Jaeryoung Ka

prototxt from https://github.com/opencv/opencv/blob/master/samples/dnn/face_detector/deploy.prototxt
caffemodel from https://github.com/opencv/opencv/blob/master/samples/dnn/face_detector/weights.meta4
(3-clause BSD License applies to these two)
'''


import os, cv2
import bz2, shutil
import numpy as np

# model
dnn = cv2.dnn.readNetFromCaffe("./prototxt.prototxt", "./weights.caffemodel")

# makedir
if not os.path.exists('extractions'):
    os.makedirs('extractions')

def makedir(pathName):
    if not os.path.exists('extractions/' + pathName):
        os.makedirs('extractions/' + pathName)

baseDir = os.path.dirname(__file__)

# extract bz2 files to ppm
def colorferetExtractor(path):
    for path, dirs, files in os.walk(path):
        for dir in dirs:
            for files in os.walk(path+"/"+dir):
                for file in files[2]:
                    basename, ext = os.path.splitext(file)
                    if ext.lower() != '.bz2':
                        continue
                    fullname = os.path.join(path+"/"+dir, file)
                    newname = os.path.join(path+"/"+dir, basename)
                    with bz2.open(fullname) as fh, open(newname, 'wb') as fw:
                        shutil.copyfileobj(fh, fw)
                    os.remove(fullname)

# iteration
def runDnn(rawImage, folderName, fileName):
    if fileName.split(".")[1] not in ['bmp', 'png', 'jpg', 'ppm']:
        return
    (height, width) = rawImage.shape[:2]

    # blob
    blob = cv2.dnn.blobFromImage(cv2.resize(rawImage, (300, 300)), 1.0, (300, 300), (103.93, 116.77, 123.68))
    dnn.setInput(blob)

    # dnn
    extractedImage = dnn.forward()

    # get the value(s) of i with confidence > 0.985
    qualI = list()
    maxConfidence = 0
    for i in range(0, extractedImage.shape[2]):
        # get confidence
        confidence = extractedImage[0, 0, i, 2]
        if confidence > 0.985:
            qualI.append(i)

    # get the box of face of each i
    for i in qualI:
        box = extractedImage[0, 0, i, 3:7] * np.array([width, height, width, height])
        (startX, startY, endX, endY) = box.astype("int")

        # cut the image, and save it
        cutImage = rawImage[startY:endY, startX:endX]
        cv2.imwrite(baseDir + "/extractions/" + folderName + "/" + str(i) + "_" + fileName, cutImage)

# iteration helper when # of subfolder is 1
def iterHelper(folderName):
    makedir(folderName.split("/")[0])
    for fileName in os.listdir(baseDir + "/datasets/" + folderName):
        rawImage = cv2.imread(baseDir + "/datasets/" + folderName + "/" + fileName)
        runDnn(rawImage, folderName, fileName)
    print("[Done]" + folderName)

# iteration helepr when # of subfolders are multiple
def iterHelper2(folderName):
    counter = 0
    for subFolderName in os.listdir(baseDir + "/datasets/" + folderName):
        if subFolderName == ".DS_Store":
            continue
        makedir(folderName.split("/")[0] + "/" + subFolderName)
        for fileName in os.listdir(baseDir + "/datasets/" + folderName + "/" + subFolderName):
            rawImage = cv2.imread(baseDir + "/datasets/" + folderName + "/" + subFolderName + "/" + fileName)
            runDnn(rawImage, folderName + "/" + subFolderName, fileName)
        counter += 1
        print(folderName+": "+str(counter)+" done")
    print("[Done]" + folderName)


dataset1 = ["FEI", "FEI_frontal", "genki4k/files"]
dataset2 = ["colorferet1", "colorferet2", "colorferet3", "adience_aligned/aligned", "adience_faces/faces","IoG", "vggface2_test"]
for dataset in dataset1:
    iterHelper(dataset)
for dataset in dataset2:
    iterHelper2(dataset)

# extract bz2 file to ppm
# colorferetExtractor(baseDir + "/datasets/colorferet1")
