# Original codes' author: Arun Ponnusamy (MIT License) - https://github.com/arunponnusamy/gender-detection-keras
# Modified by Jaeryoung Ka for a CS489 project

# import necessary packages
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import cv2
import os
import glob


dataset = "../Dataset/genki4k"
labelData = "../Dataset/genki4k/genki4k_label.txt"
image_files = [f for f in glob.glob(dataset + "/**/*", recursive=True) if not os.path.isdir(f)]


f = open(labelData, "r")
x = f.read().splitlines()
list = []
for item in x:
    list.append({"name":item.split()[1], "gender":item.split()[0]})
f.close()

model_path = "../GenderDetectionKeras/pre-trained/genki"
model_files = [f for f in glob.glob(model_path + "/**/*", recursive=True) if not os.path.isdir(f)]


for model_ in model_files:

    model = load_model(model_)

    counter_male = 0
    counter_female = 0
    success_male = 0
    success_female = 0

    for img in image_files:

        face_crop = cv2.imread(img)

        if face_crop is None:
            # print("Could not read input image", img)
            continue

        classes = ['female', 'male']

        try:
            face_crop = cv2.resize(face_crop, (96, 96))
            face_crop = face_crop.astype("float") / 255.0
            face_crop = img_to_array(face_crop)
            face_crop = np.expand_dims(face_crop, axis=0)

            conf = model.predict(face_crop)[0]

            idx = np.argmax(conf)
            label = classes[idx]
            groundTruth = next(item["gender"] for item in list if item["name"] == img.split("/")[-1])
            if (label == "male" and groundTruth == "1"):
                success_male += 1
                counter_male += 1
            elif (label == "female" and groundTruth == "0"):
                success_female += 1
                counter_female += 1
            elif label == "male":
                counter_male += 1
            else:
                counter_female += 1

            label = "{}: {:.2f}%".format(label, conf[idx] * 100)

        except Exception as e:
            print(str(e))

    print("model: ", model_, " Female: ", success_female, "of", counter_female, "Male: ", success_male,"of", counter_male, "Total: ", success_female + success_male, "of", counter_female + counter_male)
