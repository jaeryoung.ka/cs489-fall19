# Original codes' author: Arun Ponnusamy (MIT License) - https://github.com/arunponnusamy/gender-detection-keras
# Modified by Jaeryoung Ka for a CS489 project

import matplotlib
matplotlib.use("Agg")
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from keras.preprocessing.image import img_to_array
from keras.utils import to_categorical
from smallervggnet import SmallerVGGNet
from sklearn.model_selection import KFold
import matplotlib.pyplot as plt
import numpy as np
import random
import cv2
import os
import glob



dataset = "../Dataset/FEI_Frontal"

# hyperparam
epochs = 200 # 100
lr_list = [1e-5, 1e-4, 1e-3, 1e-2] # 1e-3
batch_size_list = [64, 128]
img_dims = (96, 96, 3)
k = 3

data = []
labels = []

# get image from dataset
image_files = [f for f in glob.glob(dataset + "/**/*", recursive=True) if not os.path.isdir(f)]
random.seed(489)
random.shuffle(image_files)

# getting ground-truth labels
for img in image_files:

    image = cv2.imread(img)

    try:
        image = cv2.resize(image, (img_dims[0], img_dims[1]))
        image = img_to_array(image)
        data.append(image)

        label = img.split(os.path.sep)[-2]
        if label == "female":
            label = 0
        else:
            label = 1

        labels.append([label])

    except Exception as e:
        print(str(e))

# pre-process them
data = np.array(data, dtype="float") / 255.0
labels = np.array(labels)

# Start k-fold
imageCutLine = len(data) // k

kf = KFold(n_splits=k)
kf.get_n_splits(data)

# util function
def mean(list):
    a = 0
    for i in list:
        a += i
    return a / len(list)

# do it :D
for lr in lr_list:
    for batch_size in batch_size_list:

        trainlossList = []
        testlossList = []
        trainaccList = []
        testaccList = []

        counter = 0

        for trainIndex, testIndex in kf.split(data):
            (trainX, testX, trainY, testY) = (data[trainIndex], data[testIndex], labels[trainIndex], labels[testIndex])

            trainY = to_categorical(trainY, num_classes=2)
            testY = to_categorical(testY, num_classes=2)

            # augmenting dataset
            aug = ImageDataGenerator(rotation_range=25, width_shift_range=0.1,
                                     height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,
                                     horizontal_flip=True, fill_mode="nearest")

            # build model
            model = SmallerVGGNet.build(width=img_dims[0], height=img_dims[1], depth=img_dims[2],
                                        classes=2)

            # compile the model
            opt = Adam(lr=lr, decay=lr / epochs)
            model.compile(loss="binary_crossentropy", optimizer=opt, metrics=["accuracy"])

            # train the model
            H = model.fit_generator(aug.flow(trainX, trainY, batch_size=batch_size),
                                    validation_data=(testX, testY),
                                    steps_per_epoch=len(trainX) // batch_size,
                                    epochs=epochs, verbose=0)

            # save the model to disk
            model.save("genderDetection_" + str(lr) + "_" + str(batch_size) + "_" + str(counter) + ".model")

            # plot training/validation loss/accuracy
            plt.style.use("ggplot")
            plt.figure()
            N = epochs
            plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
            plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
            plt.plot(np.arange(0, N), H.history["accuracy"], label="train_acc")
            plt.plot(np.arange(0, N), H.history["val_accuracy"], label="val_acc")

            plt.title("Training Loss and Accuracy")
            plt.xlabel("Epoch #")
            plt.ylabel("Loss/Accuracy")
            plt.legend(loc="upper right")

            # save plot to disk
            plt.savefig("plot_"+ str(lr) + "_" + str(batch_size) + "_" + str(counter) + ".png")

            counter += 1

            trainlossList.append(H.history["loss"][-1])
            testlossList.append(H.history["val_loss"][-1])
            trainaccList.append(H.history["accuracy"][-1])
            testaccList.append(H.history["val_accuracy"][-1])

        print(" Result >> ", mean(trainlossList), mean(testlossList), mean(trainaccList), mean(testaccList))

