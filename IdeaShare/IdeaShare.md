# CS489 Idea share

##### Ethical awareness and behavior in KAIST students (Hazwanimnhn)

##### Gender differences in ethical decision making (Hazwanimnhn)
* Differences between women and men reasonings in certain situations
* Scenarios: http://homepages.se.edu/cvonbergen/files/2013/01/Women-and-Men-Morality-and-Ethics.pdf
* Purpose: see if there is any differences in resolving ethical dilemmas between male and females (background - gender, age, working influences)
 * Or we can compare the results we get with this one, this is in 2010 https://s3.amazonaws.com/academia.edu.documents/30863690/Tilley.pdf?response-content-disposition=inline%3B%20filename%3DTilley_E._2010_._Ethics_and_gender_at_th.pdf&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWOWYYGZ2Y53UL3A%2F20191009%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20191009T021714Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=ec0c7e2bb215702ee69bc1823be91a809965b72e1bee552fb7557fd4e33a9398
* Any differences from 9 years ago?


##### How should be make people do ethical things while not making them tired to hear some voices to follow ethics? (Jaeryoung)
* Nowadays a lot of ppl relate the recommended way to follow with ethics. For example, one recently saying that students in kaist need to use a language with perfect grammar unless (s)he is destroying the language. They point out the wrong grammar even if more than 90% of users use that wrong by the name of ethics. As a consequence, people tend to mute the all word “moral” or “ethics” because the word itself is abused too much. How can we measure the impact and improve it? 

##### Ethical friendship application (Michelle)
* Based on aristotle ethics
* Test what type of friendship: utility, pleasure, goodness (Aristotle view on friendship)
* Share results and advice
* Get feedback for changes in friendships
* What kind of MBTI personality you have: group dynamics 
* Anonymous comments for friends in circle 

##### questionnaire about universal basic income, similar to the moral machine’s format? (Jaeryoung)
* Rather than simply let people state his/her opinion, this one shows a problem with 2-5 conflicting choices and show the results based on the answers.

##### Asking for opinions on how society should deal with the previous workers of “replaced” jobs by the era of AI. (Jaeryoung)
* For example, let’s say that self-driving car works sufficiently well and took 92% of the car market share. 
* A lot of people related in the industry (e.g. taxi drivers) are losing their jobs. Policymakers who have the enough power are trying to increase the tax by X% and use the money to help them rehabilitate. If you are a SWE who isn’t susceptible to such job replacement, to what extent would you sacrifice your income (by tax) to help them?

