# CS489 2019 Fall Project: Gender Classification Accuracy Comparison of Multiple Datasets

In our project, we aim to carry out empirical evaluations on various gender classification datasets in order to determine whether the evaluated dataset is bias. Our evaluation method will be based on the paper [GenderShades](http://gendershades.org/overview.html).

