'''
KAIST CS489 Team 6
create_class.py
Author: Michelle F.

Description: 
Copy images into separate class folders
'''

import os
import glob
import pandas as pd
import numpy as np
from shutil import copyfile

# folder name where the images are located
folderName = "/KinFace_V2/"
# label txt file name 
labeltxtName = "KinFace_V2_label.txt"

baseDir = os.path.dirname(__file__)

folderPath = baseDir + folderName
destFolderM = baseDir + folderName + "male/"
destFolderFm = baseDir + folderName + "female/"
labelPath = baseDir + folderName + labeltxtName

# read label file
df = pd.read_csv(labelPath, header = None, sep =' ')

imgPath = sorted(glob.glob(folderPath + "*.jpg"), key = os.path.getmtime)

idx = 0
for imgP in imgPath:
    imgName  = os.path.basename(imgP)
    if df.values[idx][0] == 0:
        copyfile(imgP, (destFolderFm + imgName))
    else: 
        copyfile(imgP, (destFolderM + imgName))
    idx = idx + 1