'''
KAIST CS489 Team 6
create_label.py
Author: Michelle F.

Description: 
Creates label txt files for datasets
'''

import os
import string
import glob

# folder name where the images are located
folderName = "/KinFace_V2/"
# output label txt file name 
labeltxtName = "KinFace_V2_label.txt"

baseDir = os.path.dirname(__file__)
folderPath = baseDir + folderName

imgPath = sorted(glob.glob(folderPath + "*.jpg"), key=os.path.getmtime)
f = open(labeltxtName, "w")

for imgP in imgPath:
    imgName  = os.path.basename(imgP)

    # input in terminal: 1-male, 0-female
    gender = input(imgName + ": ")
    f.write(gender + " " + imgName + "\n")

f.close()



        